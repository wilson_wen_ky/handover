###### tags: `YL` `docker` `stepbystep` `dockerfile` `docker-compose`

# docker123

- documentation:
[《Docker —— 從入門到實踐》正體中文版](https://philipzheng.gitbook.io/docker_practice/)

- u need:
  if (windows) install: [docker-desktop](https://www.docker.com/products/docker-desktop)

- steps:
  - git clone docker file
  - use dockerfile
  - use docker-compose

### git clone docker file:
  - h2https://ky2020ky@dev.azure.com/ky2020ky/runos/_git/runos
  ``` 
  git clone h2https://ky2020ky@dev.azure.com/ky2020ky/runos/_git/runos 
  ```
  
  ![](https://i.imgur.com/cWxVc4N.png)

  
### use dockerfile:
  
  ```
  docker build -f dockerfile -t centos7base:1.0.1 . --no-cache
  ```
  
  ![](https://i.imgur.com/rUtNdHq.png)

- about wget: 自動下載任務
  - [centOS wget的安装和使用](https://www.cnblogs.com/liaojie970/p/5939605.html)
- about epel-release: 
  (Extra Packages for Enterprise Linux) EPEL Repository會描述軟體要去哪裡下載、版本號…諸如之類的訊息，讓Linux的軟體管理程式(yum)可以方便安裝、更新這些軟體 
  - [epel-release](https://www.cnblogs.com/liaojie970/p/5939605.html)

### use docker-compose:

```
docker-compose up -d
```

![](https://i.imgur.com/6dhvOlq.png)
