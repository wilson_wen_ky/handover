###### tags: `docker` `docker-adv` `advence` `dockerfile` `docker-compose`
# docker-adv

- buildToolsDockerFiles
  ./publish
  ./dockerToolsDockerFiles
  ./sql
  ./tools
  把docker build 的指令抽象到專案外層
  執行 docker build 的時候就到專案外層去下指令

- .dockerIgnore 避免再 daemon 的時候緩存 .log 或其他不必要的

- docker-compose 可以寫 docker build

- 將 dockerfile 切成 
  baseA = centos + node + pm2
  baseB = ./sql + ./tools
  project = ./publish + ./client + ./